#!/bin/bash
LBSCHEMANAMES="${1:-"HR OC OE SH"}"

for LBSCHEMA in $LBSCHEMANAMES
do
    /liquibase/liquibase --changeLogFile=./main.xml --username=ORACLE_USER --password=password --url="jdbc:h2:file:./oracledb;Mode=Oracle;INIT=CREATE SCHEMA IF NOT EXISTS ${LBSCHEMA}\\;SET SCHEMA ${LBSCHEMA}" --contexts=${LBSCHEMA} --defaultSchemaName=${LBSCHEMA} status --verbose
    /liquibase/liquibase --changeLogFile=./main.xml --username=ORACLE_USER --password=password --url="jdbc:h2:file:./oracledb;Mode=Oracle;INIT=CREATE SCHEMA IF NOT EXISTS ${LBSCHEMA}\\;SET SCHEMA ${LBSCHEMA}" --contexts=${LBSCHEMA} --defaultSchemaName=${LBSCHEMA} update
    /liquibase/liquibase --changeLogFile=./main.xml --username=ORACLE_USER --password=password --url="jdbc:h2:file:./oracledb;Mode=Oracle;INIT=CREATE SCHEMA IF NOT EXISTS ${LBSCHEMA}\\;SET SCHEMA ${LBSCHEMA}" --contexts=${LBSCHEMA} --defaultSchemaName=${LBSCHEMA} history
done
