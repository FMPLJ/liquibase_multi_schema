# Liquibase_Multi_Schema
Please keep in mind the following:
1. This is a Liquibase project with H2 database with Oracle compatablity mode, but can easily become another database platform with some adjustments to the scripts.
2. The <b>liquibase.properties</b> may or may not need to be completed with the right information for the different properties.
3. Prior to running the <span style="color:blue"><b>update_schemas.sh</b></span> script make sure to do the following:<br />
  a. Please keep in mind that Liquibase will combine a schema list by a fixed list provided as an argument.  For example: <b>./update_schemas.sh "HR OC OE SH"</b><br />
  b. Each SQL file (also a formatted SQL changelog) will need to have a <b>context</b> changeset attribute with the specific <b>schema name/s</b> associated with the SQL script, so Liquibase will know to what <b>schema</b> to update.<br />
4. This example is using a master changelog "main.xml" with an <b>includeAll</b> tag pointing to a folder SQLFILES containing the SQL files.<br />
5. There is a GitLab Job associated with this repository. Please see the .gitlab-ci.yml build script for more information.
